# Introduction
This is a backend practice project using GrpahQL API and Prisma ORM.

# Setup
## Database
Make sure the config `prisma/docker-compose.yml` being well set for heroku's PostgreSQL service.
```
$ cd prisma
$ docker-compose up -d
$ prisma deploy
```

## Server
```
$ npm i
$ npm run get-schema
$ npm start
```

# GraphQL playground
Server playground is at
```
http://localhost:4000
```
ORM playground is at
```
http://localhost:4466
```
As the ORM playground is not public for access, it requires a private token which can be gotten by command `$ prisma token`. Then, set this token to the UI's header like the format `{Authorization: Bearer ${token}}`.

# Data model
There are three main data structures in this project which are `User`, `Post`, and `Comment`.
For more detail, please refer to `prisma/datamodel.graphql`.
